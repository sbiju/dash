# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

# Create your views here.


def Homepage(request):
    # qs_1 = Company.objects.all()[0:4]
    # qs_2 = Company.objects.all()[4:8]
    # offer = Offers.objects.all()
    # slider_1 = Slider.objects.all()[0:1]
    # slider_2 = Slider.objects.all()[1:2]
    return render(request, 'home.html', {
        # 'qs_1': qs_1,
        # 'qs_2': qs_2,
        # 'offer': offer,
        # 'slider_1':slider_1,
        # 'slider_2': slider_2,
    })